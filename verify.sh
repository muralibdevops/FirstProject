echo "Starting sample Ci Verification script"
echo "Trying to execute ./helloworld"	  

OUTPUT= './helloworld'
RETVAL=$?

if [$RETVAL -eq 0]; then
   echo "RETVAL is 0, OK"
else
   echo "RETVAL is not 0, FAIL"
   exit 1
fi

if ["$OUTPUT == "HELLO WORLD" ]: then   
   echo "Output is Correct , OK"
else
   echo "Output is Not right, FAIL"
   exit 1
fi   